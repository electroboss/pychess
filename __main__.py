from graphics import *
from logic import *

# board piece order
# prkbqk for white then black
board = [
  [7, 8, 9, 10,11,9, 8, 7],
  [6, 6, 6, 6, 6, 6, 6, 6],
  [-1,-1,-1,-1,-1,-1,-1,-1],
  [-1,-1,-1,-1,-1,-1,-1,-1],
  [-1,-1,-1,-1,-1,-1,-1,-1],
  [-1,-1,-1,-1,-1,-1,-1,-1],
  [0, 0, 0, 0, 0, 0, 0, 0],
  [1, 2, 3, 4, 5, 3, 2, 1]
]

while 1:
  points = getPossibleMoves(board,(4,6))
  render(board, points)
