def getPossibleMoves(board: list, currentpiece: tuple, flags: dict=None) -> list:
  """
  Get all possible moves for a specific piece given the board and some flags.
  Note: flags is not currently functional - it shall contain information such
  as whether en passant or castling is possible as those depend on previous
  moves.
  """
  piece = getPiece(board, currentpiece)
  if piece == 0: # wpawn
    if currentpiece[1] >= 7: # if pawn is at the end
      return [] # TODO: pawn promotion
    possibmoves = []
    if getPiece(board,(currentpiece[0],currentpiece[1]+1)) == -1: # if piece directly ahead is blank
      possibmoves.append((currentpiece[0],currentpiece[1]+1))
      if (currentpiece[1] == 1) and (getPiece(board,(currentpiece[0],currentpiece[1]+2)) == -1):
        # if piece hasn't moved and 2 ahead is empty
        possibmoves.append((currentpiece[0],currentpiece[1]+2))
    if getPiece(board, (currentpiece[0]+1,currentpiece[1]+1)) > 5: # if diagonal is black
      possibmoves.append((currentpiece[0]+1,currentpiece[1]+1))
    if getPiece(board, (currentpiece[0]-1,currentpiece[1]+1)) > 5: # if other diagonal is black
      possibmoves.append((currentpiece[0]-1,currentpiece[1]+1))
    return possibmoves

def getPiece(board, piece):
  if piece[1] > 8 or piece[1] < 0 or piece[0] > 8 or piece[0] < 0:
    return -2
  return board[piece[1]][piece[0]]