TILE_SIZE = 50
BOARD_X, BOARD_Y = 8,8
PIECE_IMAGES = [
  "images/wpawn50.png",
  "images/wrook50.png",
  "images/wknight50.png",
  "images/wbishop50.png",
  "images/wqueen50.png",
  "images/wking50.png",
  "images/bpawn50.png",
  "images/brook50.png",
  "images/bknight50.png",
  "images/bbishop50.png",
  "images/bqueen50.png",
  "images/bking50.png"
]

import pygame
pygame.init()
display = pygame.display.set_mode((TILE_SIZE*BOARD_X, TILE_SIZE*BOARD_Y))

pieces = []
for piece_image in PIECE_IMAGES:
  pieces.append(pygame.image.load(piece_image))

def render(board, points=[]):
  display.fill((255,255,255))

  for j,y in enumerate(board):
    for i,x in enumerate(y):
      if x == -1:
        continue
      display.blit(pieces[x],(TILE_SIZE*i,TILE_SIZE*j))
  
  for point in points:
    pygame.draw.circle(display, (255,0,0), point, TILE_SIZE//5)

  pygame.display.flip()
